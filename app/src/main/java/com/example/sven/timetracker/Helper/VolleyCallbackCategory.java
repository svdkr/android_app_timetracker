package com.example.sven.timetracker.Helper;

import com.example.sven.timetracker.Model.Category;

import java.util.ArrayList;

public interface VolleyCallbackCategory {
    void callback(ArrayList<Category> categories);
}
