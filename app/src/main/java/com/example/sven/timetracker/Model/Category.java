package com.example.sven.timetracker.Model;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.TimeZone;

public class Category {
    private int dbID;
    private int id;
    private String cname;
    private String created_at;
    private String updated_at;
    private String url;
    private String syncTask;
    private String clientTime;

    public Category(int id, String cname, String created_at, String updated_at, String url) {
        this.id = id;
        this.cname = cname;
        this.created_at = created_at;
        this.updated_at = updated_at;
        this.url = url;
    }

    public Category(int dbID, int id, String cname, String created_at, String updated_at, String url, String syncTask, String clientTime){
        this.dbID = dbID;
        this.id = id;
        this.cname = cname;
        this.created_at = created_at;
        this.updated_at = updated_at;
        this.url = url;
        this.syncTask = syncTask;
        this.clientTime = clientTime;
    }

    public void setDbID(int dbID) {
        this.dbID = dbID;
    }

    public void setClientTime(String clientTime) {
        this.clientTime = clientTime;
    }

    public String getClientTime() {
        return clientTime;
    }

    public int getDbID() {
        return dbID;
    }

    public String getSyncTask() {
        return syncTask;
    }

    public void setSyncTask(String syncTask) {
        this.syncTask = syncTask;
    }
    public void setId(int id) {
        this.id = id;
    }

    public void setCname(String cname) {
        this.cname = cname;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getId() {
        return id;
    }

    public String getCname() {
        return cname;
    }

    public String getCreated_at() {

        return created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public String getUrl() {
        return url;
    }

    public void generateClientTime(){
        Calendar cal= Calendar.getInstance(TimeZone.getTimeZone("Europe/Berlin"));
        String format = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX";
        clientTime = new SimpleDateFormat(format, Locale.GERMANY).format(cal.getTime());
    }
}

