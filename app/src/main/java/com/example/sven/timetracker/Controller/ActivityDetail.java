package com.example.sven.timetracker.Controller;

import android.app.TimePickerDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sven.timetracker.App.AppController;
import com.example.sven.timetracker.Connectivity.InternetCheck;
import com.example.sven.timetracker.Connectivity.NetworkSchedulerService;
import com.example.sven.timetracker.Helper.Constants;
import com.example.sven.timetracker.Helper.CustomCategoryAdapter;
import com.example.sven.timetracker.Helper.OfflineSyncHelper;
import com.example.sven.timetracker.Model.Activity;
import com.example.sven.timetracker.Model.Category;
import com.example.sven.timetracker.R;
import com.google.gson.Gson;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Objects;

public class ActivityDetail extends AppCompatActivity {

    private Gson gson = new Gson();
    private Activity activity;
    private ArrayList<Category> categories = new ArrayList<>();
    private CustomCategoryAdapter categoryAdapter;
    private BroadcastReceiver broadcastReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        setSupportActionBar(findViewById(R.id.my_toolbar));
        Objects.requireNonNull(getSupportActionBar()).setTitle(R.string.detail);
        Intent intent = getIntent();
        activity = gson.fromJson(intent.getStringExtra(Constants.ACTIVITY), Activity.class);
        TextView description = findViewById(R.id.editDescription);
        description.setText(activity.getDescription());

        EditText editTime = findViewById(R.id.editTime);
        editTime.setText(activity.convertStartToTime());

        initTimePicker();

        initCategorySpinner();
        loadCategoryDataOffline();
    }

    @Override
    protected void onStop(){
        stopService(new Intent(this, NetworkSchedulerService.class));
        unregisterReceiver(broadcastReceiver);
        super.onStop();
    }

    @Override
    protected void onStart(){
        Intent intent = new Intent(this, NetworkSchedulerService.class);
        broadcastReceiver();
        startService(intent);
        loadCategoryDataOffline();
        super.onStart();
    }
    @Override
    //Menubar aufbauen
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    private void initCategorySpinner() {
        Spinner spinner = findViewById(R.id.spinnerCategory);
        categoryAdapter = new CustomCategoryAdapter(this, categories);
        spinner.setAdapter(categoryAdapter);
    }

    private void loadCategoryData() {
        //laden der Kategorien
        AppController.getInstance().getRestHelper().restCategoryGet(categories -> {
            setCategoryData(categories);
            changeFirstSpinnerItem();
        });
    }

    public void onUpdateBtn(View v) {

        TextView editText = findViewById(R.id.editDescription);
        String description = editText.getText().toString();

        Spinner spinner = findViewById(R.id.spinnerCategory);
        Category category = (Category) spinner.getSelectedItem();

        EditText editTime = findViewById(R.id.editTime);
        String start = editTime.getText().toString();

        if (!description.isEmpty() && !start.isEmpty()) {

            activity.setCategory_id(category.getId());
            activity.setDescription(description);
            activity.setStart(start);

            new InternetCheck(internet -> {
                if(internet){
                    AppController.getInstance().getRestHelper().restActivityPut(activities -> {
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                    }, activity);
                }else{
                    OfflineSyncHelper.getInstance().addSyncUpdate(activity,this.getApplicationContext());
                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                }
            });

        } else {
            Toast.makeText(this, "Bitte alle Felder füllen", Toast.LENGTH_SHORT).show();
        }
    }

    public void onDeleteBtn(View view) {
        new InternetCheck(internet -> {
            if(internet){
                AppController.getInstance().getRestHelper().restActivityDelete(activities -> {
                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                }, activity);
            }else{
                OfflineSyncHelper.getInstance().addSyncDelete(activity, this.getApplicationContext());
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });
    }

    public void changeFirstSpinnerItem() {
        for (int i = 0; i < categoryAdapter.getCount(); i++) {
            Category category = categoryAdapter.getItem(i);
            if (category != null) {
                if (category.getId() == activity.getCategory_id()) {
                    Spinner spinner = findViewById(R.id.spinnerCategory);
                    spinner.setSelection(i);
                    break;
                }
            }
        }
    }
    private void setCategoryData(ArrayList<Category> categoryData){
        categoryAdapter.clear();
        categoryAdapter.addAll(categoryData);
        categoryAdapter.notifyDataSetChanged();
    }

    public void broadcastReceiver(){
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                new InternetCheck(internet -> {
                    if (internet) {
                        loadCategoryData();
                    } else {
                        loadCategoryDataOffline();
                    }
                });
            }
        };

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("networkChange");
        registerReceiver(broadcastReceiver, intentFilter);
    }

    public void loadCategoryDataOffline(){
        setCategoryData(AppController.getInstance().getSqLiteDataBaseHelper().getAllCategories());
        changeFirstSpinnerItem();
    }
    private void initTimePicker(){

        EditText edit_time = findViewById(R.id.editTime);

        edit_time.setOnClickListener(v -> {
            Calendar mcurrentTime = Calendar.getInstance();
            int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
            int minute = mcurrentTime.get(Calendar.MINUTE);
            TimePickerDialog mTimePicker;
            mTimePicker = new TimePickerDialog(ActivityDetail.this, (timePicker, selectedHour, selectedMinute) ->
                    edit_time.setText(String.format("%02d:%02d", selectedHour, selectedMinute)), hour, minute, true);
            mTimePicker.setTitle("Uhrzeit");
            mTimePicker.show();
        });

    }
    @Override
    //Synchronisation über Menubar angestoßen
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_refresh:
                new InternetCheck(internet -> {
                    if (internet) {
                        loadCategoryData();
                    } else {
                        loadCategoryDataOffline();
                    }
                });
        }
        return super.onOptionsItemSelected(item);
    }
}
