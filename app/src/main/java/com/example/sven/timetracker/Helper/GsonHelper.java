package com.example.sven.timetracker.Helper;

import com.example.sven.timetracker.Model.Activity;
import com.example.sven.timetracker.Model.Category;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;

public class GsonHelper {

    private static final String TAG = GsonHelper.class.getSimpleName();
    private static final Gson gson = new Gson();

    static Activity buildSingleActivityFromJson(JSONObject jsonObject){
        return gson.fromJson(jsonObject.toString(), Activity.class);
    }

    static ArrayList<Activity> buildActivityListFromJson(JSONArray jsonArray){
        return new ArrayList<>(Arrays.asList(gson.fromJson(jsonArray.toString(), Activity[].class)));
    }
    static String toActivityJson(Activity activity){
        return gson.toJson(activity);
    }

    static ArrayList<Category> buildCategoryListFromJson(JSONArray jsonArray) {
        return new ArrayList<>(Arrays.asList(gson.fromJson(jsonArray.toString(), Category[].class)));
    }
}
