package com.example.sven.timetracker.Controller;

import android.annotation.SuppressLint;
import android.app.TimePickerDialog;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.sven.timetracker.App.AppController;
import com.example.sven.timetracker.AsyncTask.SyncHelper;
import com.example.sven.timetracker.Connectivity.InternetCheck;
import com.example.sven.timetracker.Connectivity.NetworkSchedulerService;
import com.example.sven.timetracker.Helper.Constants;
import com.example.sven.timetracker.Helper.CustomActivityAdapter;
import com.example.sven.timetracker.Helper.CustomCategoryAdapter;
import com.example.sven.timetracker.Helper.OfflineSyncHelper;
import com.example.sven.timetracker.Model.Activity;
import com.example.sven.timetracker.Model.Category;
import com.example.sven.timetracker.R;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Objects;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {

    private static final String TAG = MainActivity.class.getSimpleName();
    private ArrayList<Activity> activities = new ArrayList<>();
    private ArrayList<Category> categories = new ArrayList<>();
    private CustomActivityAdapter activityAdapter;
    private CustomCategoryAdapter categoryAdapter;
    private BroadcastReceiver broadcastReceiver;
    private SwipeRefreshLayout swipeRefreshLayout;
    private EditText edit_time;
    private final Gson gson = new Gson();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setSupportActionBar(findViewById(R.id.my_toolbar));
        Objects.requireNonNull(getSupportActionBar()).setTitle(R.string.my_activities);
        //initialisieren der Adapter
        initActivityListView();
        initCategorySpinner();
        //Job für die Newtzwerkprüfung starten
        scheduleJob();
        //Timepicker initialisieren
        initTimePicker();
        //Swipe initialisieren
        initSwipeRefesh();
    }

    //Anlage einer Aktivität
    public void onCreateBtn(View v) {
        EditText descr = findViewById(R.id.textEditDescription);
        String description = descr.getText().toString();
        Spinner spinner = findViewById(R.id.spinnerCategory);
        Category category = (Category) spinner.getSelectedItem();
        EditText editTime = findViewById(R.id.editTime);
        String start = editTime.getText().toString();
        if (!description.isEmpty() && !start.isEmpty() && category != null) {
            Activity activity = new Activity(description, category.getId(), start);
            new InternetCheck(internet -> {
                if (internet) {
                    AppController.getInstance().getRestHelper().restActivityPost(this::setActivityData,activity);
                    //activityPOSTOnlineHelper.restActivityPostHelper(this::setActivityData, activity);
                } else {
                    OfflineSyncHelper.getInstance().addSyncCreate(activity, this.getApplicationContext());
                    loadDataOffline();
                }
            });

        } else {
            Toast.makeText(this, "Bitte alle Felder füllen", Toast.LENGTH_SHORT).show();
        }
    }

    private void scheduleJob() {

        JobInfo myJob = new JobInfo.Builder(0, new ComponentName(this, NetworkSchedulerService.class))
                .setRequiresCharging(true)
                .setMinimumLatency(1000)
                .setOverrideDeadline(2000)
                .setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY)
                .setPersisted(true)
                .build();

        JobScheduler jobScheduler = (JobScheduler) getSystemService(Context.JOB_SCHEDULER_SERVICE);
        assert jobScheduler != null;
        jobScheduler.schedule(myJob);
    }

    @Override
    protected void onDestroy() {
        stopService(new Intent(this, NetworkSchedulerService.class));
        super.onDestroy();
    }

    @Override
    //Beim wechsel der Aktivität muss Networkscheduler und Broadcastreceiver geschlossen werden
    protected void onStop() {
        stopService(new Intent(this, NetworkSchedulerService.class));
        unregisterReceiver(broadcastReceiver);
        super.onStop();
    }

    @Override
    //Beim wechsel der Activitys muss Networkscheduler und Broadcastreceiver gestartet werden
    protected void onStart() {
        super.onStart();
        Intent intent = new Intent(this, NetworkSchedulerService.class);
        broadcastReceiver();
        startService(intent);
        loadDataOffline();
    }


    //Offline - Daten über SQLite Tabelle laden
    public void loadDataOffline() {
        setActivityData(AppController.getInstance().getSqLiteDataBaseHelper().getAllActivities());
        setCategoryData(AppController.getInstance().getSqLiteDataBaseHelper().getAllCategories());
    }

    //Listview mit den Daten der Aktivitäten füllen
    private void initActivityListView() {
        ListView listView = findViewById(R.id.activityListView);
        activityAdapter = new CustomActivityAdapter(this, activities);
        listView.setAdapter(activityAdapter);
        listView.setOnItemClickListener(this);
    }

    //Kategoriespinner laden
    private void initCategorySpinner() {
        Spinner spinner = findViewById(R.id.spinnerCategory);
        categoryAdapter = new CustomCategoryAdapter(this, categories);
        spinner.setAdapter(categoryAdapter);
    }

    @Override
    //Wechsel in die Detailansicht
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Activity activity = activities.get(position);
        Intent intent = new Intent(this, ActivityDetail.class);
        intent.putExtra(Constants.ACTIVITY, gson.toJson(activity));
        startActivity(intent);
    }

    public void loadCategoryDataOnline() {
        //laden der Kategorien
        AppController.getInstance().getRestHelper().restCategoryGet(this::setCategoryData);
    }

    public void loadActivityDataOnline() {
        //laden der Aktivitäten
        AppController.getInstance().getRestHelper().restActivityGet(this::setActivityData);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    public void broadcastReceiver() {
        //Ich habe wieder Internet
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                new InternetCheck(internet -> {
                    if(internet){
                       new SyncHelper(success -> {
                          if (success){
                              loadDataOnline();
                          }else{
                              loadDataOffline();
                          }
                       });
                    }else{
                        loadDataOffline();
                    }
                });
            }
        };

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("networkChange");
        registerReceiver(broadcastReceiver, intentFilter);
    }

    //Aktivitäten neu laden
    private void setActivityData(ArrayList<Activity> activityData) {
        activityAdapter.clear();
        activityAdapter.addAll(activityData);
        activityAdapter.notifyDataSetChanged();
    }

    //Kategoriedaten neu laden
    private void setCategoryData(ArrayList<Category> categoryData) {
        categoryAdapter.clear();
        categoryAdapter.addAll(categoryData);
        categoryAdapter.notifyDataSetChanged();
    }

    @SuppressLint("DefaultLocale")
    //Timepicker initialisieren
    private void initTimePicker() {
        edit_time = findViewById(R.id.editTime);
        edit_time.setOnClickListener(v -> {
            Calendar mcurrentTime = Calendar.getInstance();
            int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
            int minute = mcurrentTime.get(Calendar.MINUTE);
            TimePickerDialog mTimePicker;
            mTimePicker = new TimePickerDialog(MainActivity.this, (timePicker, selectedHour, selectedMinute) ->
                    edit_time.setText(String.format("%02d:%02d", selectedHour, selectedMinute)), hour, minute, true);
            mTimePicker.setTitle("Uhrzeit");
            mTimePicker.show();
        });
    }

    @Override
    //Menubar aufbauen
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }
    //Synchronisation über swipe angestoßen
    private void initSwipeRefesh() {
        swipeRefreshLayout = findViewById(R.id.swiperefresh);
        swipeRefreshLayout.setOnRefreshListener(() ->
                new InternetCheck(internet -> {
                    if (internet) {
                        new SyncHelper(success -> {
                            if (success){
                                loadDataOnline();
                            }else{
                                loadDataOffline();
                            }
                        });
                    } else {
                        loadDataOffline();
                    }
                    swipeRefreshLayout.setRefreshing(false);
                })
        );
    }

    @Override
    //Synchronisation über Menubar angestoßen
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_refresh:
                new InternetCheck(internet -> {
                    if (internet) {
                        new SyncHelper(success -> {
                            if (success){
                                loadDataOnline();
                            }else{
                                loadDataOffline();
                            }
                        });
                    } else {
                        loadDataOffline();
                    }
                });
        }
        return super.onOptionsItemSelected(item);
    }

    private void loadDataOnline(){
        loadCategoryDataOnline();
        loadActivityDataOnline();
    }
}
