package com.example.sven.timetracker.Helper;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.sven.timetracker.Model.Category;
import com.example.sven.timetracker.R;

import java.util.ArrayList;
import java.util.List;

public class CustomCategoryAdapter extends ArrayAdapter<Category> {
    private Context context;
    private List<Category> categoryList;

    public CustomCategoryAdapter(@NonNull Context context, ArrayList<Category> categoryList){
        super(context, 0, categoryList);
        this.context = context;
        this.categoryList= categoryList;
    }

    @Override
    public Category getItem(int position){
        return categoryList.get(position);
    }

    @Override
    public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
        View listItem = convertView;
        if(listItem == null) listItem = LayoutInflater.from(context).inflate(R.layout.category_item, parent, false);
        TextView description = listItem.findViewById(R.id.textView_description);
        description.setText(categoryList.get(position).getCname());
        return listItem;
    }


    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent){
        View listItem = convertView;
        if(listItem == null) listItem = LayoutInflater.from(context).inflate(R.layout.category_item, parent, false);

        Category category = categoryList.get(position);

        TextView description = listItem.findViewById(R.id.textView_description);
        description.setText(category.getCname());

        return listItem;
    }

}