package com.example.sven.timetracker.Helper;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.example.sven.timetracker.App.AppController;
import com.example.sven.timetracker.Model.Activity;
import com.example.sven.timetracker.SQLITE.SQLiteDataBaseHelper;

public class OfflineSyncHelper {

    private static OfflineSyncHelper sInstance;
    private static final String TAG = OfflineSyncHelper.class.getSimpleName();

    private OfflineSyncHelper(){}

    public static synchronized OfflineSyncHelper getInstance(){
        if( sInstance == null){
            sInstance = new OfflineSyncHelper();
        }
        return sInstance;
    }

    public void addSyncDelete(Activity activity, Context context){
        if(activity.getId() != 0){ //es gibt zu dem lokalen Eintrag einen Eintrag auf dem Server
            activity.setSyncTASK("D");
            activity.setClientTime(activity.getUpdated_at());
            AppController.getInstance().getSqLiteDataBaseHelper().updateActivity(activity.getDbID(), activity);
            Toast.makeText(context, "Offline: Eintrag zum löschen vorgemerkt",Toast.LENGTH_SHORT).show();
            Log.d(TAG, "Offline: Eintrag zum löschen vorgemerkt");
            Log.d(TAG, GsonHelper.toActivityJson(activity));
        }else{
            AppController.getInstance().getSqLiteDataBaseHelper().deleteActivity(activity.getDbID());
            Toast.makeText(context, "Offline: Eintrag gelöscht, da nicht auf dem Server vorhanden",Toast.LENGTH_SHORT).show();
            Log.d(TAG,"Offline: Eintrag gelöscht, da nicht auf dem Server vorhanden");
            Log.d(TAG, GsonHelper.toActivityJson(activity));
        }
    }

    public void addSyncUpdate(Activity activity, Context context){
        if(activity.getId() != 0){ //es gibt zu dem lokalen Eintrag einen Eintrag auf dem Server
            activity.setSyncTASK("U");
            activity.generateClientTime();
            AppController.getInstance().getSqLiteDataBaseHelper().updateActivity(activity.getDbID(), activity);
            Toast.makeText(context, "Offline: Eintrag für den Update vorgemerkt",Toast.LENGTH_SHORT).show();
            Log.d(TAG, "Offline: Eintrag für den Update vorgemerkt");
            Log.d(TAG, GsonHelper.toActivityJson(activity));
        }else{
            activity.setSyncTASK("C");
            activity.generateClientTime();
            AppController.getInstance().getSqLiteDataBaseHelper().updateActivity(activity.getDbID(), activity);
            Toast.makeText(context, "Offline: Update - Eintrag noch nicht auf dem Server - Eintrag für die Anlage vorgemerkt",Toast.LENGTH_SHORT).show();
            Log.d(TAG, "Offline: Update - Eintrag noch nicht auf dem Server - Eintrag für die Anlage vorgemerkt");
            Log.d(TAG, GsonHelper.toActivityJson(activity));
        }
    }

    public void addSyncCreate(Activity activity, Context context){
            activity.setSyncTASK("C");
            activity.generateClientTime();
            AppController.getInstance().getSqLiteDataBaseHelper().addActivity(activity);
            Toast.makeText(context, "Offline: Eintrag für die Anlage vorgemerkt",Toast.LENGTH_SHORT).show();
            Log.d(TAG, "Offline: Eintrag für die Anlage vorgemerkt");
            Log.d(TAG, GsonHelper.toActivityJson(activity));
    }
}
