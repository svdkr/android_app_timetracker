package com.example.sven.timetracker.AsyncTask;
import android.os.AsyncTask;
import android.util.Log;

import com.example.sven.timetracker.App.AppController;
import com.example.sven.timetracker.Model.Activity;

import java.util.ArrayList;

public class SyncHelper extends AsyncTask<Void, Void, Boolean> {
    private Consumer mConsumer;
    private static final String TAG = SyncHelper.class.getSimpleName();

    public  interface Consumer { void accept(Boolean success); }

    public  SyncHelper(Consumer consumer) {
        mConsumer = consumer; execute();
    }

    @Override protected Boolean doInBackground(Void... voids) {
        ArrayList<Activity> activities = AppController.getInstance().getSqLiteDataBaseHelper().getAllSyncActivities();
        for(Activity activity: activities){
            switch (activity.getSyncTASK()) {
                case "C":
                    AppController.getInstance().getRestHelper().restActivitySyncPost(success -> {
                        if(!success){
                            cancel(true);
                            onPostExecute(false);
                        }
                    },activity);
                    break;
                case "D":
                    AppController.getInstance().getRestHelper().restActivitySyncDelete(success -> {
                        if(!success){
                            cancel(true);
                            onPostExecute(false);
                        }
                    },activity);
                    break;
                case "U":
                    AppController.getInstance().getRestHelper().restActivitySyncPut(success -> {
                        if(!success){
                            cancel(true);
                            onPostExecute(false);
                        }
                    },activity);
                    break;
                default:
                    Log.d(TAG, "unbekannter Sync Status");
                    break;
            }
        }
        return true;
    }

    @Override protected void onPostExecute(Boolean success) {
        mConsumer.accept(success);
    }
}
