package com.example.sven.timetracker.Helper;

public class Constants {
    public final static String CONNECTIVITY_ACTION = "android.net.conn.CONNECTIVITY_CHANGE";
    public final static String TIMETRACKER_USER = "alex";
    public final static String TIMETRACKER_PASSWORD = "time";
    public final static String ACTIVITY_GET_POST_URL = "https://powerful-refuge-97874.herokuapp.com/activities.json";
    public final static String CATEGORY_GET_POST_URL = "https://powerful-refuge-97874.herokuapp.com/categories.json";
    public final static String ACTIVITY = "ACTIVITY";
    public final static String JSON_EXTENSION = ".json";
    public final static String ACITIVTY_PLANE_URL = "https://powerful-refuge-97874.herokuapp.com/activities/";
    public final static String CREATE_OFFLINE = "/offline_create";
    public final static String UPDATE_OFFLINE = "/offline_update";
    public final static String DELETE_OFFLINE = "/offline_delete";
    public final static String API_CREATE = "api_create";
    public final static String API_DELETE = "/api_delete";
    public final static String API_UPDATE = "/api_update";
    public final static String NETWORK_STATUS = "NETWORK_STATUS";
}
