package com.example.sven.timetracker.Helper;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.sven.timetracker.Model.Activity;
import com.example.sven.timetracker.R;

import java.util.ArrayList;
import java.util.List;

public class CustomActivityAdapter extends ArrayAdapter<Activity> {
    private Context context;
    private List<Activity> activityList;

    public CustomActivityAdapter(@NonNull Context context, ArrayList<Activity> activityList){
        super(context, 0, activityList);
        this.context = context;
        this.activityList = activityList;
    }
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent){
        View listItem = convertView;
        if(listItem == null) listItem = LayoutInflater.from(context).inflate(R.layout.activity_list_item, parent, false);

        Activity activity = activityList.get(position);

        TextView description = listItem.findViewById(R.id.textView_description);
        description.setText(activity.getDescription());

        TextView datum = listItem.findViewById(R.id.textView_date);
        datum.setText(activity.convertStartToTime());

        TextView syncDescription = listItem.findViewById(R.id.syncDescription);
        ImageView sync = listItem.findViewById(R.id.syncIcon);
        if (activity.getSyncTASK() != null){
            sync.setImageDrawable(context.getDrawable(R.drawable.ic_baseline_cached_24px));

            switch (activity.getSyncTASK()) {
                case "C":
                    syncDescription.setVisibility(View.VISIBLE);
                    syncDescription.setText(R.string.sync_create);
                    break;
                case "U":
                    syncDescription.setVisibility(View.VISIBLE);
                    syncDescription.setText(R.string.sync_update);
                    break;
                case "D":
                    syncDescription.setVisibility(View.VISIBLE);
                    syncDescription.setText(R.string.sync_delete);
                    break;
                default:
                    syncDescription.setVisibility(View.GONE);
                    break;
            }
        }else{
            syncDescription.setVisibility(View.GONE);
            sync.setImageResource(0);
            syncDescription.setText("");
        }

        return listItem;
    }

}
