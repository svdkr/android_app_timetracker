package com.example.sven.timetracker.SQLITE;

import android.provider.BaseColumns;

final class FeedReaderActivity {

    private FeedReaderActivity() {
    }

    static class FeedActivity implements BaseColumns {
        static final String ID = "id";
        static final String DESCRIPTION = "description";
        static final String CATEGORY_ID = "category_id";
        static final String START = "start";
        static final String CREATED_AT = "created_at";
        static final String UPDATED_AT = "updated_at";
        static final String URL = "url";
        static final String CLIENT_TASK = "sync_task"; //C(CREATE)/U(Update)/D(Delete)
        static final String CLIENT_TIMESTAMP = "client_time";

        static final String SQL_CREATE_ENTRIES =
                "CREATE TABLE "
                        + FeedReaderDB.FeedDB.TABLE_ACTIVITY + " (" +
                        FeedActivity._ID + " INTEGER PRIMARY KEY," +
                        FeedActivity.ID + " INTEGER," +
                        FeedActivity.DESCRIPTION + " TEXT," +
                        FeedActivity.CATEGORY_ID + " INTEGER," +
                        FeedActivity.START + " DATETIME," +
                        FeedActivity.CREATED_AT + " DATETIME," +
                        FeedActivity.UPDATED_AT + " DATETIME," +
                        FeedActivity.URL + " TEXT," +
                        FeedActivity.CLIENT_TASK + " CHARACTER(1)," +
                        FeedActivity.CLIENT_TIMESTAMP + " DATETIME," +
                        "FOREIGN KEY("+FeedActivity.CATEGORY_ID +")" +" REFERENCES "+ FeedReaderDB.FeedDB.TABLE_CATEGORY+"("+ FeedReaderCategory.FeedCategory.ID+")"+ ")";

        static final String SQL_DELETE_ENTRIES =
                "DROP TABLE IF EXISTS " + FeedReaderDB.FeedDB.TABLE_ACTIVITY;
    }


}
