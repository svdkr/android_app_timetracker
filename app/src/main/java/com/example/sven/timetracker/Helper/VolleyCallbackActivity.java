package com.example.sven.timetracker.Helper;

import com.example.sven.timetracker.Model.Activity;

import java.util.ArrayList;

public interface VolleyCallbackActivity {
    void callback(ArrayList<Activity> activities);
}
