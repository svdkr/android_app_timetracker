package com.example.sven.timetracker.Connectivity;

import android.app.job.JobParameters;
import android.app.job.JobService;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;
import android.widget.Toast;

import com.example.sven.timetracker.Helper.Constants;

public class NetworkSchedulerService extends JobService implements NetworkConnectionReceiver.ConnectivityReceiverListener {

    private static final String TAG = NetworkSchedulerService.class.getSimpleName();
    private NetworkConnectionReceiver networkConnectionReceiver;

    @Override
    public void onCreate(){
        super.onCreate();
        Log.i(TAG, "Service angelegt");
        networkConnectionReceiver = new NetworkConnectionReceiver(this);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId){
        Log.i(TAG, "Start Kommando");
        return START_NOT_STICKY;
    }

    @Override
    public boolean onStartJob(JobParameters params){
        Log.i(TAG, "Connectivity Job gestartet" + networkConnectionReceiver);
        registerReceiver(networkConnectionReceiver, new IntentFilter(Constants.CONNECTIVITY_ACTION));
        return true;
    }
    @Override
    public boolean onStopJob(JobParameters params){
        Log.i(TAG,"Ende Kommando");
        unregisterReceiver(networkConnectionReceiver);
        return true;
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if (isConnected){
            Intent intent = new Intent("networkChange");
            intent.putExtra(Constants.NETWORK_STATUS, true);
            sendBroadcast(intent);
        }else{
            Toast.makeText(this, "Offline", Toast.LENGTH_SHORT).show();

        }
    }
}