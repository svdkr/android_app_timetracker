package com.example.sven.timetracker.Helper;
import android.content.Context;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.sven.timetracker.App.AppController;
import com.example.sven.timetracker.Model.Activity;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

public class RESTHelper {
    private Context context;
    private static final String TAG = RESTHelper.class.getSimpleName();

    public RESTHelper(Context context){this.context = context;}


    //DELETE
    public void restActivityDelete(final VolleyCallbackActivity callback, final Activity activity) {

        String tag_json_obj = "json_obj_req_delete";

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.DELETE, Constants.ACITIVTY_PLANE_URL + Integer.toString(activity.getId()) + Constants.API_DELETE + Constants.JSON_EXTENSION, null, response -> {
            Log.d(TAG, "Aktivität auf dem Server gelöscht");
            Toast.makeText(context, "Datensatz erfolgreich gelöscht", Toast.LENGTH_SHORT).show();
            AppController.getInstance().getSqLiteDataBaseHelper().deleteActivity(activity.getDbID());
            callback.callback(AppController.getInstance().getSqLiteDataBaseHelper().getAllActivities());
        }, error -> {
            Log.d(TAG, "DELETE an den Server gesendet - Fehler");
            OfflineSyncHelper.getInstance().addSyncDelete(activity, context);
            callback.callback(AppController.getInstance().getSqLiteDataBaseHelper().getAllActivities());
        }) {
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();
                String credentials = String.format("%s:%s", Constants.TIMETRACKER_USER, Constants.TIMETRACKER_PASSWORD);
                String authentication = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                headers.put("Authorization", authentication);
                return headers;
            }
        };

        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppController.getInstance().addToRequestQueue(jsonObjectRequest, tag_json_obj);
    }

    //GET
    public void restActivityGet(final VolleyCallbackActivity callback) {

        String tag_json_obj = "json_obj_req_get_activity";

        JsonArrayRequest jsonObjectRequest = new JsonArrayRequest(Request.Method.GET, Constants.ACTIVITY_GET_POST_URL, null, response -> {
            if (response != null) {
                AppController.getInstance().getSqLiteDataBaseHelper().purgeAndReloadActivities(GsonHelper.buildActivityListFromJson(response));
                callback.callback(AppController.getInstance().getSqLiteDataBaseHelper().getAllActivities());
                Log.d(TAG, "Aktivitäten vom Server erhalten");
            }

        }, error -> Toast.makeText(context, "Synchronisation der Aktivitäten fehlgeschlagen", Toast.LENGTH_LONG).show()) {

            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();
                String credentials = String.format("%s:%s", Constants.TIMETRACKER_USER, Constants.TIMETRACKER_PASSWORD);
                String authentication = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                headers.put("Authorization", authentication);
                return headers;
            }
        };

        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppController.getInstance().addToRequestQueue(jsonObjectRequest, tag_json_obj);

    }

    //POST
    public void restActivityPost(final VolleyCallbackActivity callback, final Activity activity) {

        String tag_json_obj = "json_obj_req_post";

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, Constants.ACITIVTY_PLANE_URL + Constants.API_CREATE + Constants.JSON_EXTENSION, null, response -> {
            Log.d(TAG, "Aktivität auf dem Server angelegt");
            Toast.makeText(context, "Datensatz erfolgreich angelegt", Toast.LENGTH_SHORT).show();
            AppController.getInstance().getSqLiteDataBaseHelper().addActivity(GsonHelper.buildSingleActivityFromJson(response));
            //SQLiteDataBaseHelper.getInstance(context).addActivity(GsonHelper.buildSingleActivityFromJson(response));
            callback.callback(AppController.getInstance().getSqLiteDataBaseHelper().getAllActivities());
        }, error -> {
            Log.d(TAG, "CREATE an den Server geschickt - Fehler");
            OfflineSyncHelper.getInstance().addSyncCreate(activity, context);
            callback.callback(AppController.getInstance().getSqLiteDataBaseHelper().getAllActivities());
        }){
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();
                String credentials = String.format("%s:%s", Constants.TIMETRACKER_USER, Constants.TIMETRACKER_PASSWORD);
                String authentication = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                headers.put("Authorization", authentication);
                return headers;
            }

            @Override
            public String getBodyContentType(){
                return "application/json";
            }

            @Override
            public byte[] getBody(){
                try {
                    Log.d(TAG, GsonHelper.toActivityJson(activity));
                    return GsonHelper.toActivityJson(activity) == null ? null : GsonHelper.toActivityJson(activity).getBytes("utf-8");
                } catch (UnsupportedEncodingException e) {
                    VolleyLog.wtf("Unsupported encoding");
                    return null;
                }
            }

        };

        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppController.getInstance().addToRequestQueue(jsonObjectRequest, tag_json_obj);
    }

    //Category PUT
    public void restActivityPut(final VolleyCallbackActivity callback, final Activity activity) {

        String tag_json_obj = "json_obj_req_put";

        Log.d(TAG,Integer.toString(activity.getId()));
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.PUT, Constants.ACITIVTY_PLANE_URL + Integer.toString(activity.getId()) + Constants.API_UPDATE + Constants.JSON_EXTENSION, null, response -> {
            Toast.makeText(context, "Datensatz erfolgreich geändert", Toast.LENGTH_SHORT).show();
            Log.d(TAG, "Aktivität auf dem Server geändert");
            AppController.getInstance().getSqLiteDataBaseHelper().updateActivity(activity.getDbID(), GsonHelper.buildSingleActivityFromJson(response));
            callback.callback(AppController.getInstance().getSqLiteDataBaseHelper().getAllActivities());
        }, error -> {
            Log.d(TAG, "PUT an den Server geschickt - Fehler");
            OfflineSyncHelper.getInstance().addSyncUpdate(activity, context);
            callback.callback(AppController.getInstance().getSqLiteDataBaseHelper().getAllActivities());

        }){
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();
                String credentials = String.format("%s:%s", Constants.TIMETRACKER_USER, Constants.TIMETRACKER_PASSWORD);
                String authentication = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                headers.put("Authorization", authentication);
                return headers;
            }

            @Override
            public String getBodyContentType(){
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody(){
                try {
                    Log.d(TAG,GsonHelper.toActivityJson(activity));
                    return GsonHelper.toActivityJson(activity) == null ? null : GsonHelper.toActivityJson(activity).getBytes("utf-8");
                } catch (UnsupportedEncodingException e) {
                    VolleyLog.wtf("Unsupported encoding");
                    return null;
                }
            }

        };

        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppController.getInstance().addToRequestQueue(jsonObjectRequest, tag_json_obj);
    }
    //Category GET
    public void restCategoryGet(final VolleyCallbackCategory callback) {

        String tag_json_obj = "json_obj_req_get_category";

        JsonArrayRequest jsonObjectRequest = new JsonArrayRequest(Request.Method.GET, Constants.CATEGORY_GET_POST_URL, null, response -> {
            if (response != null) {
                AppController.getInstance().getSqLiteDataBaseHelper().purgeAndReloadCategories(GsonHelper.buildCategoryListFromJson(response));
                //SQLiteDataBaseHelper.getInstance(context).purgeAndReloadCategories(GsonHelper.buildCategoryListFromJson(response));
                callback.callback(AppController.getInstance().getSqLiteDataBaseHelper().getAllCategories());
            }

        }, error -> Toast.makeText(context, "Synchronisation der Kategorien fehlgeschlagen", Toast.LENGTH_LONG).show()) {

            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();
                String credentials = String.format("%s:%s", Constants.TIMETRACKER_USER, Constants.TIMETRACKER_PASSWORD);
                String authentication = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                headers.put("Authorization", authentication);
                return headers;
            }
        };

        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppController.getInstance().addToRequestQueue(jsonObjectRequest, tag_json_obj);

    }

    //Activity Sync DELETE
    public void restActivitySyncDelete(final SyncCallback callback, Activity activity) {
        CountDownLatch countDownLatch = new CountDownLatch(1);

        String tag_json_obj = "json_obj_req_delete_sync";
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.DELETE,
                Constants.ACITIVTY_PLANE_URL  + Integer.toString(activity.getId()) + Constants.DELETE_OFFLINE + Constants.JSON_EXTENSION + "?client_time="+ activity.getClientTime(),
                null, response -> { countDownLatch.countDown(); callback.callback(true); }, error -> { countDownLatch.countDown(); callback.callback(false); }) {
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();
                String credentials = String.format("%s:%s", Constants.TIMETRACKER_USER, Constants.TIMETRACKER_PASSWORD);
                String authentication = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                headers.put("Authorization", authentication);
                return headers;
            }
        };

        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppController.getInstance().addToRequestQueue(jsonObjectRequest, tag_json_obj);
        try {
            countDownLatch.await();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    //Activity Sync POST
    public void restActivitySyncPost(final SyncCallback callback, Activity activity) {
        CountDownLatch countDownLatch = new CountDownLatch(1);

        String tag_json_obj = "json_obj_req_post_sync";

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST,
                Constants.ACITIVTY_PLANE_URL + Constants.CREATE_OFFLINE + Constants.JSON_EXTENSION,
                null, response -> { countDownLatch.countDown(); callback.callback(true); }, error -> { countDownLatch.countDown(); callback.callback(false); }){
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();
                String credentials = String.format("%s:%s", Constants.TIMETRACKER_USER, Constants.TIMETRACKER_PASSWORD);
                String authentication = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                headers.put("Authorization", authentication);
                return headers;
            }

            @Override
            public String getBodyContentType(){
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody(){
                try {
                    Log.d(TAG, GsonHelper.toActivityJson(activity));
                    return GsonHelper.toActivityJson(activity) == null ? null : GsonHelper.toActivityJson(activity).getBytes("utf-8");
                } catch (UnsupportedEncodingException e) {
                    VolleyLog.wtf("Unsupported encoding");
                    return null;
                }
            }
        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppController.getInstance().addToRequestQueue(jsonObjectRequest, tag_json_obj);
        try {
            countDownLatch.await();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
    //Activity Sync PUT
    public void restActivitySyncPut( final SyncCallback callback, Activity activity) {
        CountDownLatch countDownLatch = new CountDownLatch(1);

        String tag_json_obj = "json_obj_req_put_sync";

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.PUT,
                Constants.ACITIVTY_PLANE_URL + Integer.toString(activity.getId()) + Constants.UPDATE_OFFLINE + Constants.JSON_EXTENSION,
                null, response -> { countDownLatch.countDown(); callback.callback(true); }, error -> { countDownLatch.countDown(); callback.callback(false); }){
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();
                String credentials = String.format("%s:%s", Constants.TIMETRACKER_USER, Constants.TIMETRACKER_PASSWORD);
                String authentication = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                headers.put("Authorization", authentication);
                return headers;
            }

            @Override
            public String getBodyContentType(){
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody(){
                try {
                    Log.d(TAG, GsonHelper.toActivityJson(activity));
                    return GsonHelper.toActivityJson(activity) == null ? null : GsonHelper.toActivityJson(activity).getBytes("utf-8");
                } catch (UnsupportedEncodingException e) {
                    VolleyLog.wtf("Unsupported encoding");
                    return null;
                }
            }

        };

        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppController.getInstance().addToRequestQueue(jsonObjectRequest, tag_json_obj);
        try {
            countDownLatch.await();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}
