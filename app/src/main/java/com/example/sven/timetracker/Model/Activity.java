package com.example.sven.timetracker.Model;

import android.annotation.SuppressLint;
import android.text.format.Time;
import android.util.Log;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class Activity {
    private int dbID;
    private int id;
    private String description;
    private int category_id;
    private String start;
    private String created_at;
    private String updated_at;
    private String url;
    private String syncTASK;
    private String client_time;

    public Activity(int id, String description, int category_id, String start, String created_at, String updated_at, String url){
        this.id = id;
        this.description = description;
        this.start = start;
        this.category_id = category_id;
        this.created_at = created_at;
        this.updated_at = updated_at;
        this.url = url;
    }

    public Activity(int dbID, int id, String description, int category_id, String start, String created_at, String updated_at, String url, String syncTASK, String client_time){
        this.dbID = dbID;
        this.id = id;
        this.description = description;
        this.category_id = category_id;
        this.start = start;
        this.created_at = created_at;
        this.updated_at = updated_at;
        this.url = url;
        this.syncTASK = syncTASK;
        this.client_time = client_time;
    }

    public Activity(String description, int category_id , String start){
        this.description = description;
        this.category_id = category_id;
        this.start = start;
    }

    public String getSyncTASK() {
        return syncTASK;
    }

    public int getDbID() {
        return dbID;
    }

    public String getClientTime() {
        return client_time;
    }

    public void setClientTime(String client_time) {
        this.client_time = client_time;
    }

    public void setDbID(int dbID) {
        this.dbID = dbID;
    }

    public void setSyncTASK(String syncTASK) {
        this.syncTASK = syncTASK;
    }

    public int getId() {
        return id;
    }

    public String getStart() {
        return start;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public String getCreated_at() {
        return created_at;
    }

    public String getDescription() {
        return description;
    }

    public String getUrl() {
        return url;
    }

    public void setCategory_id(int category_id) {
        this.category_id = category_id;
    }

    public int getCategory_id() {
        return category_id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String convertStartToTime(){
        String format = "yyyy-MM-dd'T'HH:mm:ss.SSSX";
        @SuppressLint("SimpleDateFormat") SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format, Locale.GERMANY);
        Date date = null;
        try {
            date = simpleDateFormat.parse(start);
        } catch (ParseException e) {
            // handle exception here !
            e.printStackTrace();
            return start;
        }
        String newFormat = "HH:mm";
        @SuppressLint("SimpleDateFormat") SimpleDateFormat newDateFormat = new SimpleDateFormat(newFormat,  Locale.GERMANY);
        return newDateFormat.format(date);
    }

    @SuppressLint("SimpleDateFormat")
    public void generateClientTime(){
        Calendar cal= Calendar.getInstance(TimeZone.getTimeZone("Europe/Berlin"));
        String format = "yyyy-MM-dd'T'HH:mm:ss.SSSX";
        client_time = new SimpleDateFormat(format, Locale.GERMANY).format(cal.getTime());
        Log.d("Test", client_time);
    }
}
