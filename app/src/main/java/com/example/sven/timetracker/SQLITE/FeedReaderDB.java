package com.example.sven.timetracker.SQLITE;

class FeedReaderDB {
    private FeedReaderDB(){}

    static class FeedDB{
        static final int DB_VERSION = 1; //Needs to be incremented when Database scheme changes
        static final String DB_NAME = "TIMETRACKER"; //Database name
        static final String TABLE_ACTIVITY = "ACTIVITY"; // Table name activity
        static final String TABLE_CATEGORY = "CATEGORY"; // Table name category
    }
}
