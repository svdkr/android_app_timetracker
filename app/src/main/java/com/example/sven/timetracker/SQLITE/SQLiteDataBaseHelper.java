package com.example.sven.timetracker.SQLITE;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.sven.timetracker.Model.Activity;
import com.example.sven.timetracker.Model.Category;

import java.util.ArrayList;

public class SQLiteDataBaseHelper extends SQLiteOpenHelper {

    private final String TAG = SQLiteDataBaseHelper.class.getSimpleName();

    //private static SQLiteDataBaseHelper sInstance;
    private SQLiteDatabase writable;
    private SQLiteDatabase readable;

    public SQLiteDataBaseHelper(Context context) {
        super(context, FeedReaderDB.FeedDB.DB_NAME, null, FeedReaderDB.FeedDB.DB_VERSION);
        writable = this.getWritableDatabase();
        readable = this.getReadableDatabase();
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(FeedReaderCategory.FeedCategory.SQL_CREATE_ENTRIES);
        db.execSQL(FeedReaderActivity.FeedActivity.SQL_CREATE_ENTRIES);
        Log.d(TAG, "Datenbank angelegt");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(FeedReaderActivity.FeedActivity.SQL_DELETE_ENTRIES);
        db.execSQL(FeedReaderCategory.FeedCategory.SQL_DELETE_ENTRIES);
        onCreate(db);

        Log.d(TAG, "Datenbank geändert");
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

    public void addActivity(Activity activity) {
        ContentValues cv = new ContentValues();
        cv.put(FeedReaderActivity.FeedActivity.ID, activity.getId());
        cv.put(FeedReaderActivity.FeedActivity.DESCRIPTION, activity.getDescription());
        cv.put(FeedReaderActivity.FeedActivity.CATEGORY_ID, activity.getCategory_id());
        cv.put(FeedReaderActivity.FeedActivity.START, activity.getStart());
        cv.put(FeedReaderActivity.FeedActivity.CREATED_AT, activity.getCreated_at());
        cv.put(FeedReaderActivity.FeedActivity.UPDATED_AT, activity.getUpdated_at());
        cv.put(FeedReaderActivity.FeedActivity.URL, activity.getUrl());
        cv.put(FeedReaderActivity.FeedActivity.CLIENT_TASK, activity.getSyncTASK());
        cv.put(FeedReaderActivity.FeedActivity.CLIENT_TIMESTAMP, activity.getClientTime());
        writable.insert(FeedReaderDB.FeedDB.TABLE_ACTIVITY, null, cv);
    }

    public void updateActivity(int id, Activity activity) {
        ContentValues cv = new ContentValues();
        cv.put(FeedReaderActivity.FeedActivity.ID, activity.getId());
        cv.put(FeedReaderActivity.FeedActivity.DESCRIPTION, activity.getDescription());
        cv.put(FeedReaderActivity.FeedActivity.CATEGORY_ID, activity.getCategory_id());
        cv.put(FeedReaderActivity.FeedActivity.START, activity.getStart());
        cv.put(FeedReaderActivity.FeedActivity.CREATED_AT, activity.getCreated_at());
        cv.put(FeedReaderActivity.FeedActivity.UPDATED_AT, activity.getUpdated_at());
        cv.put(FeedReaderActivity.FeedActivity.URL, activity.getUrl());
        cv.put(FeedReaderActivity.FeedActivity.CLIENT_TASK, activity.getSyncTASK());
        cv.put(FeedReaderActivity.FeedActivity.CLIENT_TIMESTAMP, activity.getClientTime());

        String selection = FeedReaderActivity.FeedActivity._ID + " = ?";
        String[] args = {Integer.toString(id)};

        writable.update(FeedReaderDB.FeedDB.TABLE_ACTIVITY, cv, selection, args);

    }

    public void deleteActivity(int id) {
        String selection = FeedReaderActivity.FeedActivity._ID + " = ?";
        String[] args = {Integer.toString(id)};
        writable.delete(FeedReaderDB.FeedDB.TABLE_ACTIVITY, selection, args);

    }

    public ArrayList<Activity> getAllActivities() {
        ArrayList<Activity> activities = new ArrayList<>();
        Cursor cursor = readable.query(FeedReaderDB.FeedDB.TABLE_ACTIVITY, null, null, null, null, null, null);

        while (cursor.moveToNext()) {
            Activity activity = new Activity(
                    cursor.getInt(cursor.getColumnIndexOrThrow(FeedReaderActivity.FeedActivity._ID)),
                    cursor.getInt(cursor.getColumnIndexOrThrow(FeedReaderActivity.FeedActivity.ID)),
                    cursor.getString(cursor.getColumnIndexOrThrow(FeedReaderActivity.FeedActivity.DESCRIPTION)),
                    cursor.getInt(cursor.getColumnIndexOrThrow(FeedReaderActivity.FeedActivity.CATEGORY_ID)),
                    cursor.getString(cursor.getColumnIndexOrThrow(FeedReaderActivity.FeedActivity.START)),
                    cursor.getString(cursor.getColumnIndexOrThrow(FeedReaderActivity.FeedActivity.CREATED_AT)),
                    cursor.getString(cursor.getColumnIndexOrThrow(FeedReaderActivity.FeedActivity.UPDATED_AT)),
                    cursor.getString(cursor.getColumnIndexOrThrow(FeedReaderActivity.FeedActivity.URL)),
                    cursor.getString(cursor.getColumnIndexOrThrow(FeedReaderActivity.FeedActivity.CLIENT_TASK)),
                    cursor.getString(cursor.getColumnIndexOrThrow(FeedReaderActivity.FeedActivity.CLIENT_TIMESTAMP))
            );
            activities.add(activity);
        }
        cursor.close();
        return activities;
    }

    public ArrayList<Category> getAllCategories() {
        ArrayList<Category> categories = new ArrayList<>();
        Cursor cursor = readable.query(FeedReaderDB.FeedDB.TABLE_CATEGORY, null, null, null, null, null, null);
        while (cursor.moveToNext()) {
            Category category = new Category(
                    cursor.getInt(cursor.getColumnIndexOrThrow(FeedReaderCategory.FeedCategory._ID)),
                    cursor.getInt(cursor.getColumnIndexOrThrow(FeedReaderCategory.FeedCategory.ID)),
                    cursor.getString(cursor.getColumnIndexOrThrow(FeedReaderCategory.FeedCategory.CNAME)),
                    cursor.getString(cursor.getColumnIndexOrThrow(FeedReaderCategory.FeedCategory.CREATED_AT)),
                    cursor.getString(cursor.getColumnIndexOrThrow(FeedReaderCategory.FeedCategory.UPDATED_AT)),
                    cursor.getString(cursor.getColumnIndexOrThrow(FeedReaderCategory.FeedCategory.URL)),
                    cursor.getString(cursor.getColumnIndexOrThrow(FeedReaderCategory.FeedCategory.CLIENT_TASK)),
                    cursor.getString(cursor.getColumnIndexOrThrow(FeedReaderCategory.FeedCategory.CLIENT_TIME))
            );

            categories.add(category);
        }
        return categories;
    }

    public ArrayList<Activity> getAllSyncActivities() {
        ArrayList<Activity> activities = new ArrayList<>();
        String selection = FeedReaderActivity.FeedActivity.CLIENT_TASK + " IS NOT NULL";

        Cursor cursor = readable.query(FeedReaderDB.FeedDB.TABLE_ACTIVITY, null, selection, null, null, null, null);

        if (cursor != null && cursor.getCount() > 0) {
            while (cursor.moveToNext()) {
                Activity activity = new Activity(
                        cursor.getInt(cursor.getColumnIndexOrThrow(FeedReaderActivity.FeedActivity._ID)),
                        cursor.getInt(cursor.getColumnIndexOrThrow(FeedReaderActivity.FeedActivity.ID)),
                        cursor.getString(cursor.getColumnIndexOrThrow(FeedReaderActivity.FeedActivity.DESCRIPTION)),
                        cursor.getInt(cursor.getColumnIndexOrThrow(FeedReaderActivity.FeedActivity.CATEGORY_ID)),
                        cursor.getString(cursor.getColumnIndexOrThrow(FeedReaderActivity.FeedActivity.START)),
                        cursor.getString(cursor.getColumnIndexOrThrow(FeedReaderActivity.FeedActivity.CREATED_AT)),
                        cursor.getString(cursor.getColumnIndexOrThrow(FeedReaderActivity.FeedActivity.UPDATED_AT)),
                        cursor.getString(cursor.getColumnIndexOrThrow(FeedReaderActivity.FeedActivity.URL)),
                        cursor.getString(cursor.getColumnIndexOrThrow(FeedReaderActivity.FeedActivity.CLIENT_TASK)),
                        cursor.getString(cursor.getColumnIndexOrThrow(FeedReaderActivity.FeedActivity.CLIENT_TIMESTAMP))
                );

                activities.add(activity);
            }
            cursor.close();
        }
        return activities;
    }

    public void purgeAndReloadActivities(ArrayList<Activity> activities) {
        //purge Data
        writable.delete(FeedReaderDB.FeedDB.TABLE_ACTIVITY, null, null);

        for (Activity activity : activities) {
            addActivity(activity);
        }
    }

    public void purgeAndReloadCategories(ArrayList<Category> categories) {
        writable.delete(FeedReaderDB.FeedDB.TABLE_CATEGORY, null, null);

        for (Category category : categories) {
            addCategory(category);
        }
    }

    public void addCategory(Category category) {
        ContentValues cv = new ContentValues();
        cv.put(FeedReaderCategory.FeedCategory.ID, category.getId());
        cv.put(FeedReaderCategory.FeedCategory.CNAME, category.getCname());
        cv.put(FeedReaderCategory.FeedCategory.CREATED_AT, category.getCreated_at());
        cv.put(FeedReaderCategory.FeedCategory.UPDATED_AT, category.getUpdated_at());
        cv.put(FeedReaderCategory.FeedCategory.URL, category.getUrl());
        cv.put(FeedReaderCategory.FeedCategory.CLIENT_TASK, category.getSyncTask());
        cv.put(FeedReaderCategory.FeedCategory.CLIENT_TIME, category.getClientTime());
        writable.insert(FeedReaderDB.FeedDB.TABLE_CATEGORY, null, cv);
    }


}
