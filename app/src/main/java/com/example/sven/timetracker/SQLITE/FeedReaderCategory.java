package com.example.sven.timetracker.SQLITE;

import android.provider.BaseColumns;

public class FeedReaderCategory {

    private FeedReaderCategory() {
    }

    static class FeedCategory implements BaseColumns {
        static final String ID = "id";
        static final String CNAME = "cname";
        static final String CREATED_AT = "created_at";
        static final String UPDATED_AT = "updated_at";
        static final String URL = "url";
        static final String CLIENT_TASK = "sync_task"; //C(CREATE)/U(Update)/D(Delete)
        static final String CLIENT_TIME = "client_time";

        static final String SQL_CREATE_ENTRIES =
                "CREATE TABLE "
                        + FeedReaderDB.FeedDB.TABLE_CATEGORY + " (" +
                        FeedCategory._ID + " INTEGER PRIMARY KEY," +
                        FeedCategory.ID + " INTEGER," +
                        FeedCategory.CNAME + " TEXT," +
                        FeedCategory.CREATED_AT + " TEXT," +
                        FeedCategory.UPDATED_AT + " TEXT," +
                        FeedCategory.URL + " TEXT," +
                        FeedCategory.CLIENT_TASK + " CHARACTER(1)," +
                        FeedCategory.CLIENT_TIME + " TEXT)";

        static final String SQL_DELETE_ENTRIES =
                "DROP TABLE IF EXISTS " + FeedReaderDB.FeedDB.TABLE_CATEGORY;
    }
}
