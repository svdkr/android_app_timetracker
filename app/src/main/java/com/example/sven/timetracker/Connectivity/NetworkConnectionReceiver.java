package com.example.sven.timetracker.Connectivity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

public class NetworkConnectionReceiver extends BroadcastReceiver {

    private static final String TAG = NetworkSchedulerService.class.getSimpleName();

    private ConnectivityReceiverListener mConnectivityReceiverListener;

    public NetworkConnectionReceiver(ConnectivityReceiverListener listener){
        mConnectivityReceiverListener = listener;
    }

    @Override
    public void onReceive(Context context, Intent intent){
        Log.i(TAG, "Receiver gestartet");
        mConnectivityReceiverListener.onNetworkConnectionChanged(isOnline(context));
    }

    public boolean isOnline(Context context){
        ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        assert connMgr != null;
        NetworkInfo activeNetwork = connMgr.getActiveNetworkInfo();
        return (activeNetwork != null && activeNetwork.isConnectedOrConnecting());
    }

    public interface ConnectivityReceiverListener{
        void onNetworkConnectionChanged(boolean isConnected);
    }
}
